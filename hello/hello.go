package main

import (
	"fmt"
	"gitlab.com/zengsai/hello/stringutil"
)

func main() {
	fmt.Println(stringutil.Reverse("!oG ,olleH"))
}
